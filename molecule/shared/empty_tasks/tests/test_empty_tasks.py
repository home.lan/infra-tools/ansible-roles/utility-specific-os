import os
import testinfra.utils.ansible_runner


def fail(host):
    print(host.system_info.type + ' - ' + host.system_info.distribution + ' - ' + host.system_info.release)
    assert(False)


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_hosts_file(host):
    if ('linux' == host.system_info.type):
        if ('debian' == host.system_info.distribution):
            assert host.file('/tmp/wrong-debian').exists
        elif ('ubuntu' == host.system_info.distribution):
            assert host.file('/tmp/wrong-debian').exists
        else:
            fail(host)
    elif ('openbsd' == host.system_info.type):
        if ('openbsd' == host.system_info.distribution):
            assert host.file('/tmp/wrong-openbsd').exists
        else:
            fail(host)
    elif ('darwin' == host.system_info.type):
        if ('Mac OS X' == host.system_info.distribution):
            assert host.file('/tmp/wrong-darwin').exists
        else:
            fail(host)
    else:
        fail(host)
