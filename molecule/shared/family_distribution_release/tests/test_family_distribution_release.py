import os
import re
import testinfra.utils.ansible_runner


def fail(host):
    print(host.system_info.type + ' - ' + host.system_info.distribution + ' - ' + host.system_info.release)
    assert(False)


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_hosts_file(host):
    if ('linux' == host.system_info.type):
        if ('debian' == host.system_info.distribution):
            # Docker version of Debian will not include dot (.) releases (i.e.: 9 instead of 9.9).
            if (re.match(r"9|9\.[0-9]+", host.system_info.release)):
                assert host.file('/tmp/debian-9-debian-9').exists
            else:
                fail(host)
        elif ('ubuntu' == host.system_info.distribution):
            if ('18.04' == host.system_info.release):
                assert host.file('/tmp/ubuntu-18.04-ubuntu-18.04').exists
            else:
                fail(host)
        else:
            fail(host)
    elif ('openbsd' == host.system_info.type):
        if ('openbsd' == host.system_info.distribution):
            if ('6.4' == host.system_info.release):
                assert host.file('/tmp/openbsd-6.4-openbsd-6.4').exists
            else:
                fail(host)
        else:
            fail(host)
    elif ('darwin' == host.system_info.type):
        if ('Mac OS X' == host.system_info.distribution):
            if (re.match(r"10\.14\.[0-9]+", host.system_info.release)):
                assert host.file('/tmp/macosx-10.14-macosx-10.14').exists
            else:
                fail(host)
        else:
            fail(host)
    else:
        fail(host)
