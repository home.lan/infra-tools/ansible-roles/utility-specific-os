# Ansible Role: utility-specific-os

Allows the granularity of vars and/or tasks for specific operating systems.

Enable three different levels of specifications:
- `[os]-[version].yml`
- `[os].yml`
- `[os-family].yml`

## Requirements

None.

## Role variables

| Variable names                        | Default values                                | Description |
| :------------------------------------ | :-------------------------------------------- | :---------- |
| `utility_specific_os_role_path`       |                                               | **[Mandatory]** The folder path (relative or not) of the role to search vars and tasks into. Not slash ended. |
| `utility_specific_os_role_vars_path`  | `"{{ utility_specific_os_role_path }}/vars"`  | The folder path (relative or not) from which vars are found. Not slash ended. |
| `utility_specific_os_role_tasks_path` | `"{{ utility_specific_os_role_path }}/tasks"` | The folder path (relative or not) from which tasks are found. Not slash ended. |

## Dependencies

None.

## Examples

This role is only to be used inside another role. By following the "Tasks files" section below, it will look into the current role for files that match a specific pattern for the current OS of the host.

Example for Debian vars/tasks file naming:
- `debian-9.yml`
- `debian.yml`
- `debian.yml`

Example for Ubuntu vars/tasks file naming:
- `ubuntu-18.04.yml`
- `ubuntu.yml`
- `debian.yml`

Example for OpenBSD vars/tasks file naming:
- `openbsd-6.4.yml`
- `openbsd.yml`
- `openbsd.yml`

Example for macOS vars/tasks file naming:
- `macosx-10.14.yml`
- `macosx.yml`
- `darwin.yml`

### Requirements

    - name: wolfmah.utility-specific-os
      src: git@gitlab.com:home.lan/infra-tools/ansible-roles/utility-specific-os.git
      scm: git
      version: master

### Tasks file

    ---
    # Due to a bug, when assigning the variable `role_path` in the section `vars` of the module `include_role`, the value of
    # `role_path` is already referencing the included role. To counter that bug, we have to assing the `role_path` to a
    # temporary variable to then inject it inside the included role.
    # Ref: https://github.com/ansible/ansible/issues/36274
    - set_fact:
        current_role_path: "{{ role_path }}"

    - include_role:
        name: wolfmah.utility-specific-os
      vars:
        utility_specific_os_role_path: "{{ current_role_path }}"

## Ownership and License

This project uses [C4 (Collective Code Construction Contract)](https://rfc.harbec.site/spec-2/C4/) process for contributions.

This project uses the Mozilla Public License Version 2.0 (MPLv2) license, see [LICENSE](LICENSE).

The contributors are listed in [AUTHORS](AUTHORS).

To report an issue, use the GitLab project [issue tracker](https://gitlab.com/home.lan/infra-tools/ansible-roles/utility-specific-os/issues).
